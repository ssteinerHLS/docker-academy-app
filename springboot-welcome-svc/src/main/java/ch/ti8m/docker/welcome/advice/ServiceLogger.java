package ch.ti8m.docker.welcome.advice;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class ServiceLogger {

    private static Logger logger = Logger.getLogger(ServiceLogger.class.getName());

    public ServiceLogger() {}

    @Before("execution(@org.springframework.web.bind.annotation.RequestMapping * *(..))")
    public void logMethodAccessBefore(JoinPoint joinPoint) {
        logger.info("***** Starting: " + joinPoint.getSignature().toShortString() + " *****");
    }

    @AfterReturning("execution(@org.springframework.web.bind.annotation.RequestMapping * *(..))")
    public void logMethodAccessAfter(JoinPoint joinPoint) {
        logger.info("***** Completed: " + joinPoint.getSignature().toShortString() + " *****");
    }
}