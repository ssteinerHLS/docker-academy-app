package ch.ti8m.docker.welcome.controller;

import ch.ti8m.docker.welcome.api.WelcomeModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
class WelcomeController {

    private static Logger logger = Logger.getLogger(WelcomeController.class.getName());

    @RequestMapping(value = "/welcome", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public WelcomeModel welcome(@RequestParam(required = true, name = "name") String name) {
        return new WelcomeModel(String.format("Welcome %s from the welcome service", name));
    }

}