package ch.ti8m.docker.welcome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;
import java.util.logging.Logger;

/**
 * Created by thomas on 14.12.15.
 */
@SpringBootApplication
public class FrontendApplication implements CommandLineRunner {

    public static Logger logger = Logger.getLogger(FrontendApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(FrontendApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

        logger.info("Frontend started...");

    }

}
