package ch.ti8m.docker.welcome.service;

import java.util.List;

/**
 * Created by thomas on 12.03.16.
 */
public interface MessagesService {

    List<String> findMessages();

}
