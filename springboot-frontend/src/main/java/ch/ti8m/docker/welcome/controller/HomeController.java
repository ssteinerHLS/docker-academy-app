package ch.ti8m.docker.welcome.controller;

import ch.ti8m.docker.welcome.service.MessagesService;
import ch.ti8m.docker.welcome.service.WelcomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
class HomeController {

    @Autowired
    private WelcomeService welcomeService;

    @Autowired
    private MessagesService messagesService;

    @Value("${spring.datasource.url}")
    private String jdbcUrl;

    @Autowired
    private Environment environment;

    @RequestMapping("/")
    public String index(Model model) {
        String welcomeServiceUrl = environment.getProperty("WELCOME_SVC_URL", "http://localhost:8081/welcome");

        model.addAttribute("welcome", welcomeService.welcome("Docker-Academy"));
        model.addAttribute("welcomeServiceUrl", welcomeServiceUrl);
        model.addAttribute("messages", messagesService.findMessages());
        model.addAttribute("jdbcUrl", jdbcUrl);

        return "index";
    }
}