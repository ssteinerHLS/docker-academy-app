package ch.ti8m.docker.welcome.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by thomas on 12.03.16.
 */
@Service
public class MessagesServiceImpl implements MessagesService {

    private static Logger logger = Logger.getLogger(WelcomeServiceImpl.class.getName());
    private static final List<String> DEFAULT_MESSAGES = Arrays.asList("<Database not available>");

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<String> findMessages() {
        List<String> messages = new ArrayList<>();
        logger.info("Query database for messages");
        try {
            jdbcTemplate.query(
                    "SELECT id, message FROM MESSAGE",
                    (rs, rowNum) -> messages.add(rs.getString("message")));
        } catch (RuntimeException re) {
            logger.log(Level.SEVERE, "Database not available: " + re.getMessage());
            messages.addAll(DEFAULT_MESSAGES);
        }

        return messages;
    }

}
